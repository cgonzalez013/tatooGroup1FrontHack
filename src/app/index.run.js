(function() {
  'use strict';

  angular
    .module('tatooGroup1FrontHack')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
