(function () {
'use strict';

angular
	.module('tatooGroup1FrontHack')
	.controller('userProfileController', userProfileController);

		function userProfileController ($window, userProfileFactory) {
			var vm= this;


			vm.appointment= {};
			vm.appointment.description= vm.bodyPart+ vm.color+ vm.genre+ vm.alergies;

			vm.appointments= [];
			vm.tattooists= [];

			vm.listAppointment= function () {
				vm.appointments= userProfileFactory.listDates();
				vm.appointments.success(function (response){
					vm.appointments=response
				})
			};
			vm.createAppointment= function () {
				userProfileFactory.createDate(vm.appointment);
			};
			vm.obtainTattoists= function () {
				userProfileFactory.obtainTattooists(vm.tattooists);
			};
		};
	
	
})();