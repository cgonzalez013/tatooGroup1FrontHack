'use strict';

angular
	.module('tatooGroup1FrontHack')
	.factory('userProfileFactory',function ($http, BASIC_URL) {
		return {
			listDates: function (vmdates) {
		 		return $http({
			  method: 'GET',
			  url: BASIC_URL + '/appointments'
				}).success(function (response) {
					return response
			  }).error(function (response) {
			  	console.log(response)
			  	return []
				});
			},
			createDate: function (date) {
				return $http({
					method: 'POST',
					url: BASIC_URL + '/appointments',
					data: date
				}).success(function (response) {
					console.log(response)
				}).error(function (response) {
					console.log(response)
				})
			},
			obtainTattooists: function (tattooists) {
				return $http({
					method: 'GET',
					url: BASIC_URL + '/tattooists'
				}).success(function (response) {
					tattooists= response
					console.log(response)
				}).error(function (response) {
					console.log(response)
				})
			}
		};
	});