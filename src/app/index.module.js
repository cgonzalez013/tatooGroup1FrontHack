(function() {
  'use strict';

  angular
    .module('tatooGroup1FrontHack',
    	['ngAnimate',
    	 'ngTouch',
    	 'ngSanitize',
    	 'ngMessages',
    	 'ngAria',
    	 'ngResource',
    	 'ui.router',
    	 'ui.bootstrap',
    	 'toastr'
    	]
    );
})();
