(function() {
  'use strict';

  angular
    .module('tatooGroup1FrontHack')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })

      .state('mainHome', {
        url: '/mainHome',
        templateUrl: 'app/home/home.html',
        controller: 'homeController',
        controllerAs: 'home'
      })

      .state('error',{
        url:'/error',
        templateUrl: 'app/error/error.html'
      })

      .state('userProfile',{
        url:'/userProfile',
        templateUrl: 'app/userProfile/user_profile.html',
        controller: 'UserProfileController',
        controllerAs: 'profile'
      })
      ;

    $urlRouterProvider.otherwise('/');
  }

})();
