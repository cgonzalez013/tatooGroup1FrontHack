/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('tatooGroup1FrontHack')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
