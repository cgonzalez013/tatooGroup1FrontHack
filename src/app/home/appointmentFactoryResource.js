'use strict';

angular
	.module('tatooGroup1FrontHack')
	.factory('Twits',function ($resource, BASIC_URL) {
	return $resource(
				BASIC_URL + '/appointment/:id',
				{id: '@id'},
				{
					update:{
						method: 'put'
					}	
				}
				)
});	