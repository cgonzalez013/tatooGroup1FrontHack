'use strict';

angular
	.module('tatooGroup1FrontHack')
	.factory('dateFactory',function ($resource, BASIC_URL) {
		return $resource(
				BASIC_URL + '/users/:id',
				{id: '@id'},
				{
					update:{
						method: 'put'
					}
				}
				)
	});