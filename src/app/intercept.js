'use strict';

angular
	.module('tatooGroup1FrontHack')
	.config(function ($httpProvider) {
	$httpProvider.defaults.useXDomain = true;
		$httpProvider.interceptors.push(
			function ($q, $window) {
				return {
				request: function(config) {	
				config.headers = config.headers || {};
				config.headers['Content-Type'] = 'application/json';
				config.headers['Accept'] = 'json';
					if (!!$window.sessionStorage.user) {
						var user;
						user=JSON.parse($window.sessionStorage.user);
						config.headers['X-Token'] = user["token"];
					}
				 return config;
				}
			  };
			}
		);
	});