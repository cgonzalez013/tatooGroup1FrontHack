(function() {
  'use strict';

  angular
    .module('tatooGroup1FrontHack')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($state, $timeout, webDevTec, toastr, Session, $window) {
    var vm= this;
    vm.user= {
      "is_client": 1
    };
    vm.register= function () {
      Session.register(vm.user, function () {
        $state.go('userProfile');
      });
      vm.user= {};
    };
    vm.logIn= function () {
      Session.logIn(vm.user, function () {
        $state.go('userProfile');
      });
      vm.user= {};
    }
    vm.wantToShowLogIn= function () {
      if ($window.sessionStorage.showLogIn) {
        $window.sessionStorage.setItem('showLogIn', 'false')
      } else {
        $window.sessionStorage.setItem('showLogIn', 'false')
      }
    }
    vm.showLogIn= function () {
      return $window.sessionStorage.showLogIn
    }
  }
})();
