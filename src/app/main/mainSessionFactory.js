'use strict';

angular
	.module('tatooGroup1FrontHack')
	.factory('Session',function ($http, BASIC_URL, $window) {
		return {
			register : function (user, callback) {
		 		return $http({
			  method: 'POST',
			  url: BASIC_URL + '/users',
			  data: user
				}).success(function (response) {
					console.log(response)
					callback();
			  }).error(function (response) {
				console.log(response);
				});
			},

			logIn: function (user, callback) {
				return $http({
					method: 'POST',
					url: BASIC_URL + '/access',
					data: user
				}).success(function (response) {
					$window.sessionStorage.user= JSON.stringify(response);
					console.log($window.sessionStorage.user);
					callback();
				}).error(function (response) {
					console.log(response);
				});
			}
		};
	});